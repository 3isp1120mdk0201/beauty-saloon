# Beauty Saloon



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- <b>[ERD- диаграмма] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/ERD.png)</b>
<b>![ER-Diogramm](./ERD.png)</b>

-<b>[Use-case] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-10-19_145439.png)</b>
<b>![UseCase](./Снимок_экрана_2023-10-19_145439.png)</b>

-<b>[Диагамма-последовательности] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/Forvoard_diagram.png)</b>
<b>![Диагамма-последовательности](./Forvoard_diagram.png)</b>

- <b>[База данных] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-10-24_093550.png)</b>
<b>![База данных](./Снимок_экрана_2023-10-24_093550.png)</b>

- <b>[Дизайн] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-10-24_092007.png)</b>
<b>![Дизайн](./Снимок_экрана_2023-10-24_092007.png)</b>

- <b>[Структура] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-10-24_095701.png)</b>
<b>![Структура](./Снимок_экрана_2023-10-24_095701.png)</b>

- <b>[Окно авторизации] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A0%D0%B8%D1%81%D1%83%D0%BD%D0%BE%D0%BA1.png)</b>
<b>![Окно авторизации](./Рисунок1.png)</b>

- <b>[Окно регистрации] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A0%D0%B8%D1%81%D1%83%D0%BD%D0%BE%D0%BA2.png)</b>
<b>![Окно регистрации](./Рисунок2.png)</b>

- <b>[Окно изменения данных] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A0%D0%B8%D1%81%D1%83%D0%BD%D0%BE%D0%BA3.png)</b>
<b>![Окно изменения данных](./Рисунок3.png)</b>

- <b>[Главное Окно] (https://gitlab.com/3isp1120mdk0201/beauty-saloon/-/blob/main/%D0%A0%D0%B8%D1%81%D1%83%D0%BD%D0%BE%D0%BA4.png)</b>
<b>![Главное Окно](./Рисунок4.png)</b>

